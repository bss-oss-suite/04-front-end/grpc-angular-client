import { Injectable } from '@angular/core';
import { grpc } from '@improbable-eng/grpc-web';
import { Request } from '@improbable-eng/grpc-web/dist/typings/invoke';
import { AuthServiceLogin } from '../grpc/authentication_pb_service';
import { LoginRequest, LoginResponse } from '../grpc/authentication_pb';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private client: Request;

  constructor() {
  }

  login(username: string, password: string) {
    const request = new LoginRequest();
    request.setUsername(username);
    request.setPassword(password);

    this.client = grpc.invoke(AuthServiceLogin.LoginResponse, {
      request: request,
      host: `https://localhost:50051/Authentication/login`,
      onMessage: (message: LoginResponse) => {
        const data = message.toObject();
        console.log(data);
      },
      onEnd: (code: grpc.Code, msg: string | undefined, trailers: grpc.Metadata) => {
          if (code.grpc.Code.OK){
            console.log('request finished without error');
          } else {
            console.log('an error occured', code, msg, trailers);
          }
        }
    });

  }
}
